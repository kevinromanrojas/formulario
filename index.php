<?php 
$errores = '';
$enviado = '';
if (isset($_POST['submit'])) {
	$nombre=$_POST['nombre'];
	$correo=$_POST['correo'];
	$mensaje=$_POST['mensaje'];
	if (!empty($nombre)) {
		$nombre=trim($nombre);
		$nombre=filter_var($nombre, FILTER_SANITIZE_STRING); 
	} 
	else
	{
		$errores .='Ingrese un Nombre <br/>';
	}
	if (!empty($correo)) {
		$correo = filter_var($correo, FILTER_SANITIZE_EMAIL);
		if(!filter_var($correo, FILTER_VALIDATE_EMAIL))
		{
			$errores .= 'Porfavor ingrese un correo valido <br/>';

		}
	}
	else
	{
		$errores .='Porvafor ingrese un correo <br/>';
	}

	if (!empty($mensaje)) {
		$mensaje=trim($mensaje);
		$mensaje=htmlspecialchars($mensaje);
		$mensaje=stripslashes($mensaje);
	}
	else
	{
		$errores .='Porfavor ingrese un mensaje';
	}
}

require 'view/index.view.php';
?>